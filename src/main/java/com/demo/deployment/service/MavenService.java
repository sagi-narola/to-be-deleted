package com.demo.deployment.service;

import org.apache.maven.shared.invoker.*;
import org.apache.maven.shared.utils.cli.CommandLineException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.Arrays;

@Service
public class MavenService {

    String mavenHome= System.getenv("MAVEN_HOME");

    public boolean buildJar(String folderPath) {
        InvocationRequest request = new DefaultInvocationRequest();
        request.setPomFile(new File(folderPath+"/pom.xml"));
        request.setGoals(Arrays.asList( "clean", "install" ));

        Invoker invoker = new DefaultInvoker();
        invoker.setMavenHome(new File(mavenHome));
        try {
           InvocationResult result= invoker.execute(request);
           if(result.getExitCode()==1){
               return false;
           }
        } catch (MavenInvocationException e) {
            e.printStackTrace();
            return false;
        }
        return true;

    }

}
