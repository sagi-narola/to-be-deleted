package com.demo.deployment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;

@SpringBootApplication
public class DeploymentApplication {

	public static void main(String[] args){
		SpringApplication.run(DeploymentApplication.class, args);
	}

}
