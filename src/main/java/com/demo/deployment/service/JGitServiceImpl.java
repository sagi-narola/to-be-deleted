package com.demo.deployment.service;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.eclipse.jgit.api.*;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.merge.MergeStrategy;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class JGitServiceImpl implements GitService {

    @Value("${git.username}")
    private String gitUserName;
    @Value("${git.password}")
    private String gitPassword;
    @Value("${git.url}")
    private String gitUrl;
    @Value("${git.build.branch}")
    private String buildBranchName;
    @Value("${git.clone-branch-name}")
    private String cloneBranchName;

    @Autowired
     private FileService fileService;

    Logger logger= LoggerFactory.getLogger(JGitServiceImpl.class);

    public String repositoryLocalPath="cloned-project";

    @Override
    public String cloneOrPullRepo(){

        boolean folderExist=new File("cloned-project").exists();

        if(folderExist){
            pullRepository();
        }else {
            cloneRepository();
        }

        return new File(repositoryLocalPath).getAbsolutePath();
    }

    @Override
    public void pullRepository() {
        logger.info("pulling repo");

        Git git = null;
        try {
            git = Git.open(new File(repositoryLocalPath));
        } catch (IOException e) {
            e.printStackTrace();
        }

        CredentialsProvider cp = new UsernamePasswordCredentialsProvider(gitUserName, gitPassword);

        PullCommand pull = git.pull();
        pull.setCredentialsProvider(cp);
        pull.setRemoteBranchName(cloneBranchName);
        pull.setStrategy(MergeStrategy.RECURSIVE);
        pull.setRebase(true);
        PullResult result = null;
        try {
            result = pull.call();
        } catch (GitAPIException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void cloneRepository() {
        logger.info("cloning repo");
        String path="cloned-project";
        try {
            FileUtils.deleteDirectory(new File(path));
        } catch (IOException e) {
            logger.info("cannot delete already exist directory");
            e.printStackTrace();
        }

        CredentialsProvider cp = new UsernamePasswordCredentialsProvider(gitUserName, gitPassword);
        String branchToCheckout=cloneBranchName;
        try {
            Git.cloneRepository()
                    .setURI(gitUrl)
                    .setDirectory(new File(path))
                    .setBranch(branchToCheckout)
                    .setCredentialsProvider(cp)
                    .call();
        } catch (GitAPIException e) {
            fileService.deleteDirectory("cloned-project");
            e.printStackTrace();
            logger.info("error while cloning repo");
        }
    }

    public boolean commitToRepository(){
        boolean checkOutStatus=checkoutToBranch(new File(repositoryLocalPath),buildBranchName,true);
        if(!checkOutStatus || !addFileToCommit()){
           return false;
        }

        boolean commitStatus=commit(new File(repositoryLocalPath),"commit from java code");
        if(!commitStatus){
            return false;
        }
        return pushChanges(new File(repositoryLocalPath));
    }


    public boolean addFileToCommit() {
        boolean actionSuccessful = false;
            try (Git git = Git.open(new File(repositoryLocalPath))) {
                git.add().addFilepattern(".").call();
                actionSuccessful=true;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return actionSuccessful;
    }

    public boolean commit (File repositoryLocalPath, String commitMessage) {

        boolean actionSuccessful = false;

        try (Git git = Git.open(repositoryLocalPath)) {

            Status status = git.status().call();
            git.commit().setMessage(commitMessage).call();
            actionSuccessful = true;

        } catch (Exception e) {
            e.printStackTrace();
            logger.info("error while committing to repo");
        }
        return actionSuccessful;
    }

    public boolean checkoutToBranch(File repositoryLocalPath, String branchName,  boolean forceRefUpdate) {

        boolean actionCompleted = false;

        try (Git git = Git.open(repositoryLocalPath)) {
            try{
                git.checkout()
                        .setCreateBranch(true)
                        .setName(branchName)
                        .setForceRefUpdate(forceRefUpdate)
                        .call();
            }catch (Exception e){
                git.checkout()
                        .setCreateBranch(false)
                        .setName(branchName)
                        .setForceRefUpdate(forceRefUpdate)
                        .call();
            }

            actionCompleted = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return actionCompleted;
    }

    public boolean pushChanges(File repositoryLocalPath) {
        boolean actionSuccessful = false;
        CredentialsProvider cp = new UsernamePasswordCredentialsProvider(gitUserName, gitPassword);

       // Iterable<PushResult> pushResults = null;
        try (Git git = Git.open(repositoryLocalPath)) {

            PushCommand pushCommand = git.push();
            pushCommand.setRemote("origin");
            pushCommand.setCredentialsProvider(cp);
            //pushCommand.setProgressMonitor(new SimpleProgressMonitor());
            //pushCommand.setPushAll();
             pushCommand.call();
             actionSuccessful=true;
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("error while pushing to repo");
        }
        return actionSuccessful;
       // return pushResults;
    }






}
