package com.demo.deployment.service;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;

@Service
public class FileService {

    public File createDirectory(String directoryPath){
        File directory = new File(directoryPath);
        if (! directory.exists()){
            directory.mkdir();
            // If you require it to make the entire directory path including parents,
            // use directory.mkdirs(); here instead.
            return directory;
        }
        return directory;
    }

    public boolean copyFile(String sourceFileName,String destFileName){
        File sourceFile = new File(sourceFileName);
        File destFile = new File(destFileName);

        try {
            FileUtils.copyFile(sourceFile,destFile);
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public boolean deleteDirectory(String path){
        File file = new File(path);
        try {
            FileUtils.deleteDirectory(file);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
