package com.demo.deployment.service;



public interface GitService {
    String cloneOrPullRepo();
    void pullRepository();
    void cloneRepository();
    boolean addFileToCommit();
    boolean commitToRepository();

}
